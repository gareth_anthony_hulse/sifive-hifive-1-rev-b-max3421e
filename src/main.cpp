/* © Copyright 2020 Gareth Anthony Hulse
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "app/prci.hpp"
#include "app/util.hpp"
#include "app/clint.hpp"

#include <matilda/cpu/sifive/e31.hpp>
#include <cstdio>
#include <cstdint>

namespace
{
  namespace cpu = matilda::cpu::sifive::e31;
  
  cpu::gpio gpio;
  cpu::spi spi_1 (1);

  /*  void
  setup_prci ()
  {
    app::prci prci;

    prci.hfrosc_cfg (app::prci::bitmask::hfrosc_cfg::enable);

    prci.pll_cfg (app::prci::bitmask::pll_cfg::reference_select | app::prci::bitmask::pll_cfg::select  | 0b1 | 39 << 4 | 1 << 10);
    app::util::delay (app::util::seconds (3U));
  }
  */
  void
  setup_spi ()
  {
    spi_1.csdef (0xffffffff);
    spi_1.csid (0b1);
    spi_1.delay0 (0b0, 0b0);
    spi_1.delay1 (0b0 , 0b0);

    spi_1.fmt (cpu::spi::bitmask::fmt::proto::single, 0, 0, 8);
    spi_1.sckdiv (79); // cpu_clock_speed / (target_frequency * 2) - 1
    spi_1.csmode (cpu::spi::bitmask::csmode::mode::off);
    gpio.iof_en (1 << 2 | 1 << 8 | 1 << 9 | 1 << 10); // Enable SPI.
    app::util::delay (app::util::seconds (1U));
  }

  struct max3421e
  {
    struct reg
    {
      static const uint8_t
      pinctl = 1 << 7 | 1 << 3,
	mode = 1 << 7 | 1 << 6 | 1 << 4 | 1 << 3,
	hirq = 1 << 7 | 1 << 6 | 1 << 3,
	revision = 1 << 7 | 1 << 4,
	usbctl = 0b01111000,
	usbirq = 1 << 6 | 1 << 5 | 1 << 3;
    };
    
    struct bitmask
    {
      static const uint8_t write = 0b010;

      struct pinctl
      {
	static const uint8_t fdupspi = 1 << 4;
      };

      struct mode
      {
	static const uint8_t host = 0b1,
	  speed = 0b10;
      };

      struct usbctl
      {
	static const uint8_t chipres = 1 << 5;
      };

      struct usbirq
      {
	static const uint8_t oscokirq = 0b1;
      };
    };
  };
}

int
main ()
{
  //setup_prci (); // Comment out for now, until UART is sorted out.
  setup_spi ();

  // `spi_1.csdef (0xfffffffe)` enables the MAX3421E.
  // `spi_1.csdef (0xffffffff)` disables the MAX3421E.

  // Config MAX3421E:  
  spi_1.csdef (0xfffffffe);
  spi_1.txdata (max3421e::reg::pinctl | max3421e::bitmask::write); // Select 'PINCTL' register to be written.
  spi_1.txdata (max3421e::bitmask::pinctl::fdupspi); // Set 'FDUPSPI' high.
  spi_1.csdef (0xffffffff);

  // Reset:
  spi_1.csdef (0xfffffffe);
  spi_1.txdata (max3421e::reg::usbctl | max3421e::bitmask::write); // Select 'USBCTL' register to be written.
  spi_1.txdata (max3421e::bitmask::usbctl::chipres); // Enable `CHIPRES`.
  spi_1.csdef (0xffffffff);

  spi_1.csdef (0xfffffffe); 
  spi_1.txdata (max3421e::reg::usbctl | max3421e::bitmask::write); // Select 'USBCTL' register to be written.
  spi_1.txdata (0b0); // Clear register.
  spi_1.csdef (0xffffffff); // Chip disable.

  app::util::delay (app::util::seconds (1U));
  
  while (not (spi_1.txdata (max3421e::reg::usbirq) & max3421e::bitmask::usbirq::oscokirq)); // Wait until chip is ready.

  spi_1.csdef (0xfffffffe); 
  spi_1.txdata (max3421e::reg::mode | max3421e::bitmask::write); // Select 'MODE' register to be written.
  spi_1.txdata (max3421e::bitmask::mode::host | max3421e::bitmask::mode::speed); // Set 'SPEED' and 'HOST' bit high.
  spi_1.csdef (0xffffffff); // Chip disable.
  
  while (1)
    {
      /* To read from the chip, send the first byte with the register we want to read, then get the value of `rxdata`.
       * After that, send the second byte with the value `0b0`.
       */

      // Read registers:
      spi_1.csdef (0xfffffffe);
      uint8_t data = spi_1.txdata (max3421e::reg::pinctl);
      spi_1.txdata (0b0);
      spi_1.csdef (0xffffffff);
	    
      std::printf ("PINCTL: %u\n", data);
      
      spi_1.csdef (0xfffffffe);
      data = spi_1.txdata (max3421e::reg::mode);
      spi_1.txdata (0b0);
      spi_1.csdef (0xffffffff);
      
      std::printf ("MODE: %u\n", data);
      
      spi_1.csdef (0xfffffffe);
      data = spi_1.txdata (max3421e::reg::hirq);
      spi_1.txdata (0b0);
      spi_1.csdef (0xffffffff);

      std::printf ("HIRQ: %u\n", data);

      spi_1.csdef (0xfffffffe);
      data = spi_1.txdata (max3421e::reg::revision);
      spi_1.txdata (0b0);
      spi_1.csdef (0xffffffff);
      
      std::printf ("REVISION: %u\n", data);

      spi_1.csdef (0xfffffffe);
      data = spi_1.txdata (max3421e::reg::revision);
      spi_1.txdata (0b0);
      spi_1.csdef (0xffffffff);
      
      std::printf ("REVISION: %u\n", data);

      spi_1.csdef (0xfffffffe);
      data = spi_1.txdata (max3421e::reg::revision);
      spi_1.txdata (0b0);
      spi_1.csdef (0xffffffff);
      
      std::printf ("REVISION: %u\n", data);

      spi_1.csdef (0xfffffffe);
      data = spi_1.txdata (max3421e::reg::revision);
      spi_1.txdata (0b0);
      spi_1.csdef (0xffffffff);
      
      std::printf ("REVISION: %u\n", data);

      app::util::delay (app::util::seconds (1U));
    }

  
  return 0;
}
