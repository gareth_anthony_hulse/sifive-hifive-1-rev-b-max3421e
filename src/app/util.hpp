/* © Copyright 2020 Gareth Anthony Hulse
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "core.hpp"
#include "clint.hpp"

#include <cmath>
#include <cstdio>

namespace app
{
  class util
  {
  private:
    inline static app::clint m_clint;

  public:
    template <typename T, typename = typename std::enable_if<std::is_unsigned<T>::value, T>::type>
    static constexpr void
    delay (T time)
    {
      time = time + m_clint.mtime ();
      while (m_clint.mtime () < time);
    }

    template <typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type>
    static constexpr T
    seconds (T time)
    {
      return time * std::pow (10, 9) / app::clint::clock;
    }

    template <typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type>
    static constexpr T
    miliseconds (T time)
    {
      return time * std::pow (10, 6) / app::clint::clock;
    }
  };
}
