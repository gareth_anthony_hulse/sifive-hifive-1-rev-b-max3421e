/* © Copyright 2020 Gareth Anthony Hulse
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <array>

namespace app
{  
  class pwm
  {
  private:
    int m_instance;
    
    std::array<int, 3>
    base =
      {
	0x10015000,
	0x10025000,
	0x10035000
      };

  public:
    pwm (const int instance = 0)
    {
      m_instance = instance;
    }
    
    struct bitmask
    {
      enum class cfg : uint32_t
      {
	enable_always = 1 << 12,
	  cmp_1_center = 1 << 17,
	  deglitch = 1 << 10,
	  sticky = 1 << 8
      };
    };
    
    constexpr void
    cfg (const bitmask::cfg data, const uint8_t divider)
    {
      volatile int *address = reinterpret_cast<volatile int *> (base[m_instance]);
      if (static_cast<uint32_t> (data) > 0b1111 and divider < 0b1111)
	{
	  *address = static_cast<uint32_t> (data) + divider;
	}
    }

    constexpr void
    cmp_1 (const int data)
    {
      volatile int *address = reinterpret_cast<volatile int *> (base[m_instance] + 0x24);
      *address = data;
   }
  };
}

constexpr app::pwm::bitmask::cfg operator
| (app::pwm::bitmask::cfg a, app::pwm::bitmask::cfg b)
{
  return static_cast<app::pwm::bitmask::cfg> (static_cast<uint32_t> (a) | static_cast<uint32_t> (b));
}
